{
  description = "A basic flake with a shell and git-gamble";

  inputs.nixpkgs.url = "github:NixOS/nixpkgs/nixpkgs-unstable";

  inputs.flake-utils.url = "github:numtide/flake-utils";

  inputs.git-gamble.url = "gitlab:pinage404/git-gamble";
  inputs.git-gamble.inputs.nixpkgs.follows = "nixpkgs";
  inputs.git-gamble.inputs.flake-utils.follows = "flake-utils";

  outputs = { self, nixpkgs, flake-utils, git-gamble }:
    flake-utils.lib.eachDefaultSystem
      (system:
        let
          pkgs = nixpkgs.legacyPackages."${system}";
        in
        {
          devShell = pkgs.mkShell {
            packages = [
              pkgs.bashInteractive
              pkgs.watchexec
              pkgs.gnumake
              git-gamble.packages."${system}".git-gamble
            ];
          };
        })
    //
    {
      templates = {
        elm = {
          description = "ELM with formatting, linting and test";
          path = ./elm;
        };

        haskell = {
          description = "Haskell with formatting, linting and test";
          path = ./haskell;
        };

        nix = {
          description = "Nix with formatting, linting and test";
          path = ./nix;
        };

        node_typescript_jest = {
          description = "TypeScript with formatting and test (Jest) on NodeJS";
          path = ./node_typescript_jest;
        };

        python = {
          description = "Python with formatting, typing and test";
          path = ./python;
        };

        rust = {
          description = "Rust with formatting, linting and test";
          path = ./rust;
        };

        shell = {
          description = "Bash with formatting, linting and test";
          path = ./shell;
        };
      };
    };
}
