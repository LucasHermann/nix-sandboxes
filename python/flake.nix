{
  description = "Python with formatting, typing and test";

  inputs.nixpkgs.url = "github:NixOS/nixpkgs/nixpkgs-unstable";

  inputs.flake-utils.url = "github:numtide/flake-utils";

  inputs.pypi-deps-db.url = "github:DavHau/pypi-deps-db";
  inputs.pypi-deps-db.flake = false;

  inputs.mach-nix.url = "github:DavHau/mach-nix";
  inputs.mach-nix.inputs.nixpkgs.follows = "nixpkgs";
  inputs.mach-nix.inputs.flake-utils.follows = "flake-utils";
  inputs.mach-nix.inputs.pypi-deps-db.follows = "pypi-deps-db";

  outputs = { self, nixpkgs, flake-utils, pypi-deps-db, mach-nix }:
    flake-utils.lib.eachDefaultSystem (system:
      let
        pkgs = nixpkgs.legacyPackages."${system}";
        pythonEnv = mach-nix.lib."${system}".mkPython {
          python = "python310";

          requirements = builtins.readFile ./requirements.txt;
        };
        pythonPackages = pkgs.python310Packages;
        pythonImpureTools = [
          pythonPackages.venvShellHook
        ];
      in
      {
        devShell = pkgs.mkShell {
          packages = [
            pythonEnv
            pythonImpureTools
          ];

          venvDir = "./.venv";
        };
      });
}
