{
  description = "Nix with formatting, linting and test";

  inputs.nixpkgs.url = "github:NixOS/nixpkgs/nixpkgs-unstable";

  inputs.flake-utils.url = "github:numtide/flake-utils";

  inputs.nixt.url = "github:nix-community/nixt";
  inputs.nixt.inputs.nixpkgs.follows = "nixpkgs";
  inputs.nixt.inputs.flake-utils.follows = "flake-utils";

  outputs = { self, nixpkgs, flake-utils, nixt }:
    flake-utils.lib.eachDefaultSystem (system:
      let
        pkgs = nixpkgs.legacyPackages."${system}";
      in
      {
        devShell = pkgs.mkShell {
          packages = [
            pkgs.nixpkgs-fmt
            pkgs.statix
            # pkgs.nix-linter # broken
            nixt.packages."${system}".nixt

            pkgs.rnix-lsp # needed by VSCode extension
          ];
        };
      });
}
